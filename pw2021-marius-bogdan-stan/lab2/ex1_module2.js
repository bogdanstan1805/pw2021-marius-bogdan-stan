const myModule = require('./ex1_module1.js');

const sum2 = (vec, parNum) => {
    const parityVec = vec.filter(nr => nr % 2 == parNum % 2);
    return myModule.sum1(parityVec);
}

module.exports = {
    sum2
}
//array = [1, 2, 3, 4];
//parNum = 3;
//console.log(sum2(array, parNum));