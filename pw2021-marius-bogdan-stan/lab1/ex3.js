let dateObj = new Date();

let hours = dateObj.getHours();
let minutes = dateObj.getMinutes();
let secs = dateObj.getSeconds();
let date = dateObj.getDate();
let month = dateObj.getMonth() + 1;
let year = dateObj.getFullYear(); 

console.log(date + "-" + month + "-" + year + "\n" + hours + ":" + minutes + ":" + secs);