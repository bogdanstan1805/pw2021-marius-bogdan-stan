const moment = require('moment');

const td = () => {
    return moment().format('YYYY-MM-DD hh:mm');
}

module.exports = {
    td  
};

//console.log(td());