const express = require('express');

const myModule = require('./ex3.js');

const app = express();

app.get('/', (req, res) => {
    res.send(myModule.td());
});

app.listen(3000);