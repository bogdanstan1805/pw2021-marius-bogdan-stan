function res(x) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(x);
        }, 2000);
    });
} 

async function func() {
    const arr = [1, 2, 3, 4];

    for (nr in arr){
        var x = await res(nr);
        console.log(x);
    }
}
    func();
