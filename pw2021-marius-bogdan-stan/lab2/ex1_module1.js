const sum1 = (array) => {
    return array.reduce((a, b) => a + b, 0)
};

module.exports = {
    sum1
}

//array = [1, 2, 3, 4]
//console.log(sum1(array));